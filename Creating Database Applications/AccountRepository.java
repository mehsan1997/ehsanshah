package com.example.accessingdatajpa;

import java.util.List;

import org.springframework.data.repository.CrudRepository;


public interface AccountRepository extends CrudRepository<Account, Long>{
	
	
	List<Account> findByBalanceBetween(int i, int j);
	
	
	List<Account> findByNameStartingWith(String name);
	

	List<Account> findById(long id);

}
