package lab2;

import java.lang.String;
import java.lang.System;

public class Person { 
			
		private String name;
		private String surname;
		private int age;
		private String gender;
		private String language;
	
	public void displayName() {
		System.out.println("My name is "+ name);
	}
	
	public String toString() {
		return "Person [name=" + name + ", surname=" + surname + ", age=" + age + ", gender=" + gender + ", language="
				+ language + "]";
	}
	public String getName() {
		return name;
	}
	public Person(String name, String surname, int age, String gender, String language) {
		this.name = name;
		this.surname = surname;
		this.age = age;
		this.gender = gender;
		this.language = language;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
}