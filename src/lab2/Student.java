package lab2;

public class Student extends Person {
	
	public Student(String name, String surname, int age, String gender, String language) {
		super(name, surname, age, gender, language);

	}

	private String grade;

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}
	
	public void displayName() {
			super.displayName();
	}
}