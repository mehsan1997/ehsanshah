package lab2;

import java.lang.String;
import java.lang.System;

public class Teacher extends Person {

	public Teacher(String name, String surname, int age, String gender, String language) {
		super(name, surname, age, gender, language);
	}

	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	private String profession;

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}
	
	public void displayName() {
		
		System.out.print("I am a" + profession + title +".");
		super.displayName();
	
	}
	
}
