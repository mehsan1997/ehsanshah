package lab1;
public class Car {

	private String name;
	private String type;
	private int date_of_made;
	private int cost;
	private String color;
	
	public Car(String name)
	{
	 this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getDate_of_made() {
		return date_of_made;
	}
	public void setDate_of_made(int date_of_made) {
		this.date_of_made = date_of_made;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	

}